#!/bin/bash

#  ============LICENSE_START===============================================
#  Copyright (C) 2023. S.K. Chaudhary. All rights reserved.
#  ========================================================================
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#  ============LICENSE_END=================================================
#



#start callBack server
if [[ ${A1_VERSION} == "STD"* ]]; then
    echo "Path to callBack.py: "$PWD
    python -u callBack.py &
fi

#start near-rt-ric-simulator
echo "Path to main.py: "$PWD
python -u main.py
