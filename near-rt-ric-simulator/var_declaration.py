#  ============LICENSE_START===============================================
#  Copyright (C) 2023, S. K. Chaudhary. All rights reserved.
#  ========================================================================
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#  ============LICENSE_END=================================================
#
policy_instances = {}
policy_status = {}
callbacks = {}
forced_settings = {}
forced_settings['code']=None
forced_settings['delay']=None
policy_fingerprint={}
hosts_set=set()
