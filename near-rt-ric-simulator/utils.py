#  ============LICENSE_START===============================================
#  Copyright (C) 2023, S. K. Chaudhary. All rights reserved.
#  ========================================================================
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#  ============LICENSE_END=================================================
#

#Calculate a fingerprint from sorted items of a dict
def calcFingerprint(p):
  m=''
  if (p is dict):
    for i in sorted (p.keys()):
      m = m+str(i)+calcFingerprint(p[i])
  else:
    return str(p)
  return m