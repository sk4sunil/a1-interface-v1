#  ============LICENSE_START===============================================
#  Copyright (C) 2023, S. K. Chaudhary. All rights reserved.
#  ========================================================================
#  
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#  ============LICENSE_END=================================================

#

import json
from flask import Flask, request, Response

app = Flask(__name__) #NOSONAR

#Check alive function
@app.route('/', methods=['GET'])
def test():

  return Response("OK", 200, mimetype='text/plain')

#Receive status (only for testing callbacks)
#/statustest
@app.route('/statustest', methods=['POST', 'PUT'])
def statustest():
  try:
    data = request.data
    data = json.loads(data)
  except Exception:
    return Response("The status data is corrupt or missing.", 400, mimetype='text/plain')

  return Response(json.dumps(data), 200, mimetype='application/json')

port_number = 2222

app.run(port=port_number, host="127.0.0.1", threaded=False)